import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder


df = pd.DataFrame([
    ['green', 'M', 10.1, 'class1'],
    ['red', 'L', 13.5, 'class2'],
    ['blue', 'XL', 15.3, 'class1']]
)

df.columns = ['color', 'size', 'price', 'classlabel']

# size mapping
size_mapping = {
    'XL': 3,
    'L': 2,
    'M': 1
}

df['size'] = df['size'].map(size_mapping)

print('data with size_mapping')
print(df)

####

X = df[['color', 'size', 'price']].values
color_le = LabelEncoder()
X[:, 0] = color_le.fit_transform(X[:, 0])

print(X)
