import numpy as np
import pandas as pd

df = pd.DataFrame([
    ['green', 'M', 10.1, 'class1'],
    ['red', 'L', 13.5, 'class2'],
    ['blue', 'XL', 15.3, 'class1']]
)

df.columns = ['color', 'size', 'price', 'classlabel']

print('data')
print(df)

# size mapping
size_mapping = {
    'XL': 3,
    'L': 2,
    'M': 1
}

df['size'] = df['size'].map(size_mapping)

print('data with size_mapping')
print(df)


class_mapping = {label: idx for idx,
                 label in enumerate(np.unique(df['classlabel']))}
print('class mapping')
print(class_mapping)

df['classlabel'] = df['classlabel'].map(class_mapping)
print('df with class_mapping')
print(df)

###

inv_class_mapping = {v: k for k, v in class_mapping.items()}
df['classlabel'] = df['classlabel'].map(inv_class_mapping)
print('df with inv_class_mapping')
print(df)
