from sklearn.linear_model import LogisticRegression
from sklearn import datasets
from sklearn.cross_validation import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from matplotlib.colors import ListedColormap
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

########

df_wine = pd.read_csv('https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data', header=None)

df_wine.columns = ['Class label', 'Alcohol', 'Malic acid', 'Ash',  'Alcalinity of ash', 'Magnesium', 'Total phenols', 'Flavanoids', 'Nonflavanoid phenols',  'Proanthocyanins', 'Color intensity', 'Hue', 'OD280/OD315 of diluted wines', 'Proline']


X, y = df_wine.iloc[:, 1:].values, df_wine.iloc[:, 0].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

sc = StandardScaler()
sc.fit(X_train)
X_train_std = sc.transform(X_train)
X_test_std = sc.transform(X_test)


###

lr = LogisticRegression(penalty='l1', C=0.1)
lr.fit(X_train_std, y_train)

print('Class labels', np.unique(df_wine['Class label']))
print('df_wine.head()')
print(df_wine.head())
print('Training accuracy:', lr.score(X_train_std, y_train))
print('Test accuracy:', lr.score(X_test_std, y_test))

print(lr.intercept_)  # [-0.38376784 -0.15808088 -0.70036763]

## Output

# Class labels [1 2 3]
# df_wine.head()
#    Class label  Alcohol  Malic acid   Ash  Alcalinity of ash  Magnesium  \
# 0            1    14.23        1.71  2.43               15.6        127
# 1            1    13.20        1.78  2.14               11.2        100
# 2            1    13.16        2.36  2.67               18.6        101
# 3            1    14.37        1.95  2.50               16.8        113
# 4            1    13.24        2.59  2.87               21.0        118

#    Total phenols  Flavanoids  Nonflavanoid phenols  Proanthocyanins  \
# 0           2.80        3.06                  0.28             2.29
# 1           2.65        2.76                  0.26             1.28
# 2           2.80        3.24                  0.30             2.81
# 3           3.85        3.49                  0.24             2.18
# 4           2.80        2.69                  0.39             1.82

#    Color intensity   Hue  OD280/OD315 of diluted wines  Proline
# 0             5.64  1.04                          3.92     1065
# 1             4.38  1.05                          3.40     1050
# 2             5.68  1.03                          3.17     1185
# 3             7.80  0.86                          3.45     1480
# 4             4.32  1.04                          2.93      735
# Training accuracy: 0.983870967742
# Test accuracy: 0.981481481481
# [-0.38381387 -0.15805956 -0.70044096]

