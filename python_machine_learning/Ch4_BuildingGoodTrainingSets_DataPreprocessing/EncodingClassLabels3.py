import numpy as np
import pandas as pd
from sklearn.preprocessing import LabelEncoder


df = pd.DataFrame([
    ['green', 'M', 10.1, 'class1'],
    ['red', 'L', 13.5, 'class2'],
    ['blue', 'XL', 15.3, 'class1']]
)

df.columns = ['color', 'size', 'price', 'classlabel']

print('data')
print(df)


###

class_le = LabelEncoder()
y = class_le.fit_transform(df['classlabel'].values)

print('label')
print(y)   # [0 1 0]

inv_y = class_le.inverse_transform(y)
print('inverse label')
print(inv_y) # ['class1' 'class2' 'class1']
