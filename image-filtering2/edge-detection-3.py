# openCV
# install openCV pip install opencv-python
import cv2
import numpy as np
import matplotlib.pyplot as plt
from skimage import io, color
from skimage import exposure
img = io.imread('cat.jpg')    # Load the image
# img = color.rgb2gray(img)       # Convert the image to grayscale (1 channel)
kernel = np.array([[-1,-1,-1],[-1,8,-1],[-1,-1,-1]])
image_filtered = cv2.filter2D(img,-1,kernel)
plt.imshow(image_filtered)
plt.axis('off')
plt.show()