import numpy as np

a = np.random.randn(4, 3)
b = np.random.randn(3, 2)
c = a * b

print c.shape 

# Traceback (most recent call last):
#   File "ex03.py", line 5, in <module>
#     c = a * b
# ValueError: operands could not be broadcast together with shapes (4,3) (3,2)

