import numpy as np

# 1
a = np.random.randn(4, 3)
b = np.random.randn(4, 1)
c = a * b

print c.shape #(4, 3)

# 2
a = np.random.randn(4, 3)
b = np.random.randn(4, 3)
c = a * b

print c.shape #(4, 3)

# 3
# a = np.random.randn(4, 3)
# b = np.random.randn(3, 1)
# c = a * b

# print c.shape #(4, 3)
# Traceback (most recent call last):
#   File "ex04.py", line 20, in <module>
#     c = a * b
# ValueError: operands could not be broadcast together with shapes (4,3) (3,1)


# 4
# a = np.random.randn(4, 3)
# b = np.random.randn(4, 2)
# c = a * b

# print c.shape 

# Traceback (most recent call last):
#   File "ex04.py", line 32, in <module>
#     c = a * b
# ValueError: operands could not be broadcast together with shapes (4,3) (4,2)